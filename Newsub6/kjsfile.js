const numbers = {
  *[Symbol.matchAll] (str) {
    for (const n of str.matchAll(/[0-9]+/g))
      yield n[0];
  }
};
console.log(Array.from("2021-05-12".matchAll(numbers)));